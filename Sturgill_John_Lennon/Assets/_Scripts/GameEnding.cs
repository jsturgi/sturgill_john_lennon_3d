﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;




public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f; // sets fade effect duration
    public float displayImageDuration = 1f;
    public GameObject player; // references player
    bool m_IsPlayerCaught; // bool that checks if player is caught
    bool m_IsPlayerAtExit; // player trigger bool
    public CanvasGroup exitBackgroundImageCanvasGroup; // honestly not sure what this does?? soemthing to do with fading the canvas group??
    public CanvasGroup caughtBackgroundImageCanvasGroup; // references canvas group for when player is caught
    float m_Timer;
    public AudioSource exitAudio; // references exit sound effect
    public AudioSource caughtAudio; // references gameover audio
    bool m_HasAudioPlayed; // boolean that checks if audio has played
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_IsPlayerAtExit) // checks if player at exit bool is true
        {
            endLevel(exitBackgroundImageCanvasGroup, false, exitAudio); // calls end level function
        }

        else if (m_IsPlayerCaught) // checks if player caught bool is true
        {
            endLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio); // calls end level function
        }
        
    }

    private void OnTriggerEnter(Collider other) // triggers if game object enters collider
    {
        if (other.gameObject == player) // checks if game object is player
        {
            m_IsPlayerAtExit = true; // sets player at exit boool to true
        }
    }

    void endLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource) // ends level
    {
        if (!m_HasAudioPlayed) // checks if bool is false
        {
            audioSource.Play(); // plays audio
            m_HasAudioPlayed = true; // sets bool to true
        }
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration; // sets length of win graphic fade effect
        if(m_Timer > fadeDuration + displayImageDuration) // checks if win image has been displayed long enough
        {
            if (doRestart) // checks if restart boolean is true
            {
                SceneManager.LoadScene(0); // reloads scene
            }
            else
            {
                Application.Quit(); // closes game
            }
                
        }

    }

    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }
}
