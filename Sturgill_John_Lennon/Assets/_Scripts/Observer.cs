﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public Transform player; // references player transform
    public GameEnding gameEnding; // references GameEnding class

    bool m_IsPlayerInRange; // bool that references if player is in range

    private void OnTriggerEnter(Collider other) // trigger method that checks if player is in enemy line of sight
    {
        if (other.transform == player) // checks if game object is player
        {
            m_IsPlayerInRange = true; // sets if player is in range boolean
        }
    }

    private void OnTriggerExit(Collider other) // trigger method that checks if game object left collider
    {
        if(other.transform == player) // checks if game object is player
        {
            m_IsPlayerInRange = false; // sets player is in range boolean
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_IsPlayerInRange) // checks if m_IsPlayerInRange bool is true
        {
            Vector3 direction = player.position - transform.position + Vector3.up; // creates direction vector variable
            Ray ray = new Ray(transform.position, direction);  // creates a ray based on direction variable and enem position
            RaycastHit raycastHit; // creates RaycastHit variable
            if (Physics.Raycast(ray, out raycastHit))
            {
                if(raycastHit.collider.transform == player)
                {
                    gameEnding.CaughtPlayer();
                }
            }
        }
    }
}
