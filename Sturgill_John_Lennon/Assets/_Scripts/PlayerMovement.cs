﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f; // creates public float variable for turn speed
    Animator m_Animator; // creates player animator variable
    Rigidbody m_Rigidbody; // declares a rigidbody variable
    Vector3 m_Movement; // creates player movement vector
    Quaternion m_Rotation = Quaternion.identity; // creates a player rotation variable
    public AudioSource footsteps; // references audio source
    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>(); // references player animator
        m_Rigidbody = GetComponent<Rigidbody>(); // references rigid body
        footsteps = GetComponent<AudioSource>(); // references audio source
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal"); // checks for player input on horizontal axis
        float vertical = Input.GetAxis("Vertical"); // checks for player input on vertical axis

        m_Movement.Set(horizontal, 0f, vertical); // takes player input and plugs into movement vector
        m_Movement.Normalize(); // normalizes player speed so that the player is not faster while moving diagonally

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f); // bool that checks for player input on horizontal axis
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f); // bool that checks for player input on vertical axis

        bool isWalking = hasHorizontalInput || hasVerticalInput; // bool that checks vertical + horizontal axis for player input simultaneously
        m_Animator.SetBool("IsWalking", isWalking); // sets IsWalking bool in animator in unity based on script isWalking bool

        if (isWalking)
        {
            if (!footsteps.isPlaying)
            {
                footsteps.Play();
            }
        }
        else
        {
            footsteps.Stop();
        }
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f); // calculates player forward vector
        m_Rotation = Quaternion.LookRotation(desiredForward); // sets rotation variable to desired forward's rotation vector
    }

    private void FixedUpdate()
    {
        
    }

    private void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);  // calculates player movement vector
        m_Rigidbody.MoveRotation(m_Rotation); // rotates player
    }
}
