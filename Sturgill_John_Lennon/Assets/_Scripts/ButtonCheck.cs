﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonCheck : MonoBehaviour
{
    public GameObject textBox; // references text box

    public GameObject buttons; // references buttons

    public GameObject enemy; // references gargoyle that is removed on correct answers

    public GameEnding gameOver; // references game end scene

    public GameObject ghost; // references ghost

    public float lives = 3.0f; // references player lives

    
    public void FirstAnswerCorrect()
    {
        enemy.gameObject.SetActive(false); // removes first gargoyle
        textBox.gameObject.SetActive(false); // deactivates question text
        buttons.gameObject.SetActive(false); // deactivates buttons
        ghost.gameObject.SetActive(false); // deactivates ghost

    }

    public void SecondAnswerCorrect()
    {
        enemy.gameObject.SetActive(false); // removes second gargoyle
        textBox.gameObject.SetActive(false); // deactivates question text
        buttons.gameObject.SetActive(false); // deactivates buttons
        ghost.gameObject.SetActive(false); // deactivates ghost

    }

    public void ThirdAnswerCorrect()
    {
        enemy.gameObject.SetActive(false); // hides third gargoyle
        textBox.gameObject.SetActive(false); // deactivates question text
        buttons.gameObject.SetActive(false); // deactivates buttons
        ghost.gameObject.SetActive(false); // deactivates ghost
    }

    public void AnswerWrong()
    {
        lives = lives - 1.0f; // takes away one life per wrong answer

        if (lives == 0.0f) // checks if lives are at 0
        {
            textBox.gameObject.SetActive(false); // deactivates question
            buttons.gameObject.SetActive(false); // deactivates buttons
            gameOver.CaughtPlayer(); // calls game over scene
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
