﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent; // references nav mesh agent component
    public Transform[] waypoints; // declares an array of Transforms
    public Transform player; // references player transform
    public Collider johnLemon;
    

    int m_CurrentWaypointIndex; // stores current index of waypoint array
                                // Start is called before the first frame update

   // private void OnTriggerEnter(Collider other)
   // {
   //     navMeshAgent.velocity = Vector3.zero;
   // }

    void Start()
    {
        navMeshAgent.SetDestination(waypoints[0].position); // sets initial destination of nav mesh agent
    }

    // Update is called once per frame
    void Update()
    {
        if(navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance) // checks if nav mesh agent has arrived at destination
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length; // returns to first waypoint after going to all waypoints
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position); // sets new waypoint destination
        }

        //OnTriggerEnter(johnLemon);
        
    }

    
 }

