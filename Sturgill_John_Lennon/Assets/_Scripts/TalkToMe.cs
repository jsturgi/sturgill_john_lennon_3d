﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;


public class TalkToMe : MonoBehaviour
// requires a trigger collider added to the object this script is on
// make sure player is tagged 'player'
{
    public Text textBox; // drag and drop reference to a text object we created in a canvas in the scene (Text gameobject should start inactive

    public string toSay; // public string of what the char says, can be diff on each ghost

    public GameObject buttons; // reference to buttons game object

    public NavMeshAgent ghost; // references ghost

    

    




    private void OnTriggerEnter(Collider other)
    { // when our trigger collider sphere runs into anything
        if (other.gameObject.CompareTag("Player")) // if thing we run into is tagged player
        {
            textBox.gameObject.SetActive(true); // make text visible
            textBox.text = toSay; // set text string to what we say
            buttons.gameObject.SetActive(true); // set buttons visible
            ghost.enabled = false; // disables navmeshagent so ghost stops
            
        }
    }
}

   

































     
